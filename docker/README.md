# Docker

## build-docker-ecr.yml

- Builds docker images using multistage building pattern
- Two tier base and production
- Base should contain image/os dependencies (i.e. `apt-get` stuffs)
- Production should be app binaries and dependencies
- Uses kaniko, and the built-in layer-images-as-cache

| var                             | default                                                             | description                          |
| ------------------------------- | ------------------------------------------------------------------- | ------------------------------------ |
| IS_LATEST_TAG_ENABLED           | "no"                                                                | push latest tag                      |
| LOCALVAR_TARGET_PRODUCTION      | "production"                                                        | docker multistage target for prd img |
| LOCALVAR_DOCKER_IMAGE_NAME_NEXT | "${LOCALVAR_ECR_URL}/${LOCALVAR_DOCKER_IMAGE_NAME}:$(cat .version)" | Full image name and tag to be pushed |
| LOCALVAR_KANIKO_CACHE_REPO      | "${CI_REGISTRY_IMAGE}/cache"                                        | Registry to store kaniko cache       |

### Breakdown of Kaniko runtime flags used

```
/kaniko/executor
--cache=true # enables caching by saving layers into the repo
--cache-repo "${LOCALVAR_ECR_URL}/${LOCALVAR_DOCKER_IMAGE_NAME}" # affixes cache repo without subdirectories (needed for ECR)
--context "${LOCALVAR_DOCKER_IMAGE_CONTEXT}" # Absolute path of Docker context
--dockerfile "${LOCALVAR_DOCKER_FILE_LOCATION}" # Absolute path of Dockerfile
--target "${LOCALVAR_TARGET_PRODUCTION}" # Dockerfile build target
--destination "${LOCALVAR_DOCKER_IMAGE_NAME_NEXT}" # Full destination image and tag to be pushed
--cleanup # cleanup filesystem after building image - important if the container is being reused to build another image
```

#### Optional flags

- [`--no-push`](https://github.com/GoogleContainerTools/kaniko#flag---no-push) - build but don't push image - useful for
  testing image is builable
- [`--build-arg`](https://github.com/GoogleContainerTools/kaniko#flag---build-arg) - pass in ARG at build time

#### Why are we using the `debug` image?

We are using the debug image due to gitlab ci/cd requirements.

> The kaniko debug image is recommended (gcr.io/kaniko-project/executor:debug) because it has a shell, and a shell is
> required for an image to be used with GitLab CI/CD.
> The entrypoint needs to be overridden, otherwise the build script doesn’t run.

https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#building-a-docker-image-with-kaniko

## Trivy

.container_scanning section allow us to extend the required step in the main pipeline to scan the container image.

`.container_scanning` is broken down into 6 step,

- `--version` which print the version
- cache cleanup is needed when scanning images with the same tags, it does not remove the database
- update vulnerabilities db
- Builds report and puts it in the default workdir `$CI_PROJECT_DIR`, so `artifacts:` can take it from there
- Prints full report in the job GUI
- Fail with exit code 1 if there are any high or critical vulnerabilities, we choose to capture only high and critical
  vulnerabilities for now else it might be too sensitive.

`.sbom` is broken down into 4 step,

- `--version` which print the version
- cache cleanup is needed when scanning images with the same tags, it does not remove the database
- update vulnerabilities db
- If above stage is cleared of any critical or high CVE then we generate sbom in "cyclonedx" format, you can change to "
  spdx-json" if needed.

Trivy Doc (Do take note to change the newest version, the documentation is constantly updated):
https://aquasecurity.github.io/trivy/v0.38/docs/

| var               | default                                                         | description                                                                                                                                                                                                            |
| ----------------- | --------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| GIT_STRATEGY      | "none"                                                          | We are testing only images from public or private container repository, it is set to none by default. If you have .trivyignore in the root of repo, overwrite the variable with "GIT_STRATEGY: fetch" in the pipeline. |
| TRIVY_USERNAME    | "AWS"                                                           | default ecr login                                                                                                                                                                                                      |
| TRIVY_PASSWORD    | "$(aws ecr get-login-password --region ${LOCALVAR_AWS_REGION})" | use aws cli command to get the password                                                                                                                                                                                |
| TRIVY_AUTH_URL    | "${LOCALVAR_ECR_URL}"                                           | default ecr url                                                                                                                                                                                                        |
| TRIVY_NO_PROGRESS | "true"                                                          | do not show the container image download progress...                                                                                                                                                                   |
| TRIVY_CACHE_DIR   | ".trivycache/"                                                  | the cache path allows caching of downloaded vulnerabilities database, shared between jobs with `cache:`                                                                                                                |

## retag-docker-ecr.yml (Deprecated)

Uses DIND as a service, to perform the tag management.

Currently unused, and deprecated, as DIND requires privileged mode, which is discouraged.

- Made to complement the `tag-release` architecture
  - i.e. manual git tag env -> release by retagging image to env

| var                              | default | description                               |
| -------------------------------- | ------- | ----------------------------------------- |
| IS_RETAG_UAT_DOCKER_DISABLED     | "no"    | tag docker image as `-uat`                |
| IS_RETAG_PRD_DOCKER_DISABLED     | "no"    | tag docker image as `-prd`                |
| IS_AUTO_RETAG_UAT_DOCKER_ENABLED | "no"    | auto tag image without manual git tagging |
| IS_AUTO_RETAG_PRD_DOCKER_ENABLED | "no"    | auto tag image without manual git tagging |
