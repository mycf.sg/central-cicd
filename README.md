# CICD Centralised Repo

Opionated centralised CICD repo.

> Please ensure that you're using Gitlab >=12.2

## Quick Start

To start using the common stages, add the the include block in your `gitlab-ci.yml`.

```yaml
include:
  # Use this if you always want the latest bundled version of cicd
  # No guarantee that the latest version would always be stable
  - remote: https://gitlab.com/mycf.sg/central-cicd/-/raw/master/common/latest-bundle.yml
```

`latest-bundle.yml` are bundles of stages/jobs that pulls in the latest versions of each stage/job relevant to the framework/module. This is a limitation of `include remote` from GitLab. I highly recommend to import each type of stage/job you would like to use by specifying the version number.

The `common` module should be mostly stable and you can just use the `latest-bundle.yml`, for other modules, include by version to protect your sanity. **Do not** include more than one `latest-bundle.yml` to avoid circular includes.

```yaml
include:
  - remote: https://gitlab.com/mycf.sg/central-cicd/-/raw/master/common/latest-bundle.yml
  - remote: https://gitlab.com/mycf.sg/central-cicd/-/raw/0.0.5/nodejs/check.yml
  - remote: https://gitlab.com/mycf.sg/central-cicd/-/raw/0.0.5/nodejs/build-docker.yml
```

## Common

Default Common stages included  
(you _must_ have these stages declared in your ci)

```yaml
stages:
  - cicd version bump
  - cicd create version badge
```

- Refer to [common/variables.yml](common/variables.yml) for stage toggle flags variables.
- Set to `"yes"` to disable

1. Create deploy keys with write access under `Settings > CI / CD > Deploy Keys` / `Settings > Repository > Deploy Keys`
2. You must add these title/key into your environment under `Settings > CI / CD > Variables`
   - CIVAR_DEPLOYER_ID
   - CIVAR_DEPLOYER_TOKEN
3. For minor version bump, create a `bump_minor` label and use it during your merge request
4. For major version bump, create a `bump_major` label and use it during your merge request

## NodeJS

Default NodeJS stages included  
(you _must_ have these stages declared in your ci)

```yaml
stages:
  - cicd version metadata
  - cicd checks
  - cicd build docker
  - cicd version bump
  - cicd create version badge
```

- Refer to [nodejs/latest-bundle.yml](nodejs/latest-bundle.yml) for stage toggle flags variables.
- Set to `"yes"` to disable

## Golang

Default Golang stages included  
(you _must_ have these stages declared in your ci)

```yaml
stages:
  - cicd version metadata
  - cicd version bump
  - cicd create version badge
```

- Refer to [golang/latest-bundle.yml](golang/latest-bundle.yml) for stage toggle flags variables.
- Set to `"yes"` to disable

## Deploy

### ArgoCD

Default Deploy stages included  
(you _must_ have these stages declared in your ci)

```yaml
stages:
  - cicd deploy to qa
  - cicd deploy to uat
```

- Refer to [deploy/argocd.yml](common/variables.yml) for stage toggle flags variables.
- Set to `"yes"` to disable

## Docker build

> Assumption that all projects are using multi-stage build with base and production

- Base are all the compile time dependencies that do not need to be bundled into final production build
- Using [kaniko](https://github.com/GoogleContainerTools/kaniko)
- [Docs](docker/README.md)

## Variables

TODO: migrate to individual template folders

| **variable**                   | **module** | **stage**                    | **required** | **default**                    |
| ------------------------------ | ---------- | ---------------------------- | ------------ | ------------------------------ |
| CIVAR_DEPLOYER_ID              | common     | cicd version metadata        | yes          |                                |
| CIVAR_DEPLOYER_TOKEN           | common     | cicd version bump            | yes          |                                |
| OPS_TRIGGER_TOKEN              | deploy     | cicd deploy to qa/uat        | yes          |                                |
| LOCALVAR_DEPLOYED_SERVICE_NAME | deploy     | cicd deploy to qa/uat        | yes          |                                |
| LOCALVAR_NPM_AUDIT_ARGS        | nodejs     | cicd checks                  | no           | --audit-level=high --only=prod |
| LOCALVAR_DOCKER_IMAGE_NAME     | nodejs     | cicd build docker for qa     | yes          |                                |
| LOCALVAR_DOCKER_FILE_LOCATION  | nodejs     | cicd build docker for qa     | no           | `${CI_PROJECT_DIR}/Dockerfile` |
| LOCALVAR_DOCKER_IMAGE_CONTEXT  | nodejs     | cicd build docker for qa     | no           | `${CI_PROJECT_DIR}`            |
| LOCALVAR_DOCKER_DEV_IMAGE_NODE | nodejs     | cicd test                    | no           | node:12.8-alpine               |
| LOCALVAR_AWS_REGION            | nodejs     | cicd ecr build docker for qa | no           | ap-southeast-1                 |
| LOCALVAR_AWS_ECR_URL           | nodejs     | cicd ecr build docker for qa | yes          |                                |
| LOCALVAR_TARGET_PRODUCTION     | docker     | cicd ecr build docker        | yes          | production                     |

## Job toggles

TODO: migrate to individual template folders

| **job toggles**                     | **module**    |
| ----------------------------------- | ------------- |
| IS_VERSION_METADATA_DISABLED        | common        |
| IS_VERSION_BUMP_DISABLED            | common        |
| IS_CREATE_VERSION_BADGE_DISABLED    | common        |
| IS_INIT_DISABLED                    | nodejs        |
| IS_BUILD_DOCKER_DISABLED            | nodejs        |
| IS_BUILD_DOCKER_QA_DISABLED         | nodejs        |
| IS_BUILD_DOCKER_UAT_DISABLED        | nodejs        |
| IS_BUILD_DOCKER_STAGING_DISABLED    | nodejs        |
| IS_BUILD_DOCKER_PRODUCTION_DISABLED | nodejs        |
| IS_DEPENDENCIES_CHECK_DISABLED      | nodejs        |
| IS_UNIT_TEST_DISABLED               | nodejs        |
| IS_LINT_DISABLED                    | nodejs        |
| IS_DEPLOY_QA_DISABLED               | deploy/argocd |
| IS_DEPLOY_UAT_DISABLED              | deploy/argocd |

## Semver

- Modified from <https://github.com/mrooding/gitlab-semantic-versioning>
- Built locally, pushed to Gitlab Container Registry

### Misc. notes

Markdown tables generated with: [Markdown Table Generator](https://jakebathman.github.io/Markdown-Table-Generator/)
