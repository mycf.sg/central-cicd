#!/usr/bin/env python3
import os
import re
import sys
import semver
import subprocess
import gitlab

def git(*args):
    return subprocess.check_output(["git"] + list(args))

def verify_env_var_presence(name):
    if name not in os.environ:
        raise Exception(f"Expected the following environment variable to be set: {name}")

def extract_gitlab_url_from_project_url():
    project_url = os.environ['CI_PROJECT_URL']
    project_path = os.environ['CI_PROJECT_PATH']

    return project_url.split(f"/{project_path}", 1)[0]

def extract_merge_request_id_from_commit():
    message = git("log", "-1", "--pretty=%B")
    matches = re.search(r'(\S*\/\S*!)(\d+)', message.decode("utf-8"), re.M|re.I)
    
    if matches == None:
        raise Exception(f"Unable to extract merge request from commit message: {message}")

    return matches.group(2)

def retrieve_labels_from_merge_request(merge_request_id):
    project_id = os.environ['CI_PROJECT_ID']
    gitlab_private_token = os.environ['CIVAR_DEPLOYER_TOKEN']

    gl = gitlab.Gitlab(extract_gitlab_url_from_project_url(), private_token=gitlab_private_token)
    gl.auth()

    project = gl.projects.get(project_id)
    merge_request = project.mergerequests.get(merge_request_id)

    return merge_request.labels

def bump(latest):

    minor_bump_label = os.environ.get("MINOR_BUMP_LABEL") or "bump-minor"
    major_bump_label = os.environ.get("MAJOR_BUMP_LABEL") or "bump-major"

    try:
        merge_request_id = extract_merge_request_id_from_commit()
        labels = retrieve_labels_from_merge_request(merge_request_id)
        if minor_bump_label in labels:
            return semver.bump_minor(latest)
        elif major_bump_label in labels:
            return semver.bump_major(latest)
        else:
            return semver.bump_patch(latest)
    except:
        return semver.bump_patch(latest)


def tag_repo(tag):
    repository_url = os.environ["CI_REPOSITORY_URL"]
    username = os.environ["CIVAR_DEPLOYER_ID"]
    password = os.environ["CIVAR_DEPLOYER_TOKEN"]

    push_url = re.sub(r'([a-z]+://)[^@]*(@.*)', rf'\g<1>{username}:{password}\g<2>', repository_url)

    git("remote", "set-url", "--push", "origin", push_url)
    git("tag", tag)
    git("push", "origin", tag)        

def main():
    env_list = ["CI_REPOSITORY_URL", "CI_PROJECT_ID", "CI_PROJECT_URL", "CI_PROJECT_PATH", "CIVAR_DEPLOYER_ID", "CIVAR_DEPLOYER_TOKEN"]
    [verify_env_var_presence(e) for e in env_list]

    try:
        latest = git("tag", "--list", "--sort=-version:refname", "--sort=-creatordate").decode().partition('\n')[0]
    except subprocess.CalledProcessError:
        # Default to version 1.0.0 if no tags are available
        version = "1.0.0"
    else:
        version = bump(latest)


    f = open(".current", "w")
    if 'latest' in locals():
        f.write(latest)
        print("Old version: ", latest)
    else:
        f.write("1.0.0")
    f.close()

    f = open(".version", "w")
    f.write(version)
    f.close()

    f = open(".next", "w")
    f.write(version)
    f.close()

    tag_repo(version)

    print("New version: ", version)

    return 0


if __name__ == "__main__":
    sys.exit(main())
