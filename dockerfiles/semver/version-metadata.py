#!/usr/bin/env python3
import os
import re
import sys
import semver
import subprocess
import gitlab

def git(*args):
    return subprocess.check_output(["git"] + list(args))

def verify_env_var_presence(name):
    if name not in os.environ:
        raise Exception(f"Expected the following environment variable to be set: {name}")

def bump(latest):

    minor_bump_label = os.environ.get("MINOR_BUMP_LABEL") or "bump-minor"
    major_bump_label = os.environ.get("MAJOR_BUMP_LABEL") or "bump-major"

    if "CI_MERGE_REQUEST_LABELS" not in os.environ:
        return semver.bump_patch(latest)
        
    labels = os.environ["CI_MERGE_REQUEST_LABELS"].split(",")

    if minor_bump_label in labels:
        return semver.bump_minor(latest)
    elif major_bump_label in labels:
        return semver.bump_major(latest)

def tag_repo(tag):
    repository_url = os.environ["CI_REPOSITORY_URL"]
    username = os.environ["CIVAR_DEPLOYER_ID"]
    password = os.environ["CIVAR_DEPLOYER_TOKEN"]

    push_url = re.sub(r'([a-z]+://)[^@]*(@.*)', rf'\g<1>{username}:{password}\g<2>', repository_url)

    git("remote", "set-url", "--push", "origin", push_url)
    git("tag", tag)
    git("push", "origin", tag)        

def main():
    env_list = ["CI_REPOSITORY_URL", "CI_PROJECT_ID", "CI_PROJECT_URL", "CI_PROJECT_PATH", "CIVAR_DEPLOYER_ID", "CIVAR_DEPLOYER_TOKEN"]
    [verify_env_var_presence(e) for e in env_list]

    try:
        latest = git("describe", "--abbrev=0", "--tags").decode().strip()
    except subprocess.CalledProcessError:
        # Default to version 1.0.0 if no tags are available
        version = "1.0.0"
        latest = "1.0.0"
    
    version = bump(latest)

    f = open(".current", "w")
    f.write(latest)
    f.close()

    f = open(".version", "w")
    f.write(version)
    f.close()

    f = open(".next", "w")
    f.write(version)
    f.close()

    print("Current: ", latest)
    print("Next: ", version)

    return 0


if __name__ == "__main__":
    sys.exit(main())
